openList = ["[", "{", "("]
closeList = ["]", "}", ")"]


def balance(myStr):
    stack = []
    for i in myStr:
        if i in openList:
            stack.append(i)
        elif i in closeList:
            pos = closeList.index(i)
            if (len(stack) > 0) and (openList[pos] == stack[len(stack) - 1]):
                stack.pop()
            else:
                return "No"
    if len(stack) == 0:
        return "Yeap"


input_array = [
 '() [] () ([]()[])',
 '( (] ([)]',
 '{}[]()',
 '{{}]}',
 '{[}',
 '[}]'
]


result = []
for element in input_array:
    result.append(balance(element))
print(result)
